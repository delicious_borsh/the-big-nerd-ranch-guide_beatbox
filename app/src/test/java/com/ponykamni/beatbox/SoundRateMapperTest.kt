package com.ponykamni.beatbox

import org.junit.Assert.assertEquals
import org.junit.Test

class SoundRateMapperTest {

    private val subject = SoundRateMapper()

    @Test
    fun minValues() {
        val input = 0
        val expectedOutput = 0.5f

        val observerOutput = subject.mapRate(input)

        assertEquals(expectedOutput, observerOutput)
    }

    @Test
    fun defaultValues() {
        val input = 50
        val expectedOutput = 1.0f

        val observerOutput = subject.mapRate(input)

        assertEquals(expectedOutput, observerOutput)
    }

    @Test
    fun maxValues() {
        val input = 100
        val expectedOutput = 2.0f

        val observerOutput = subject.mapRate(input)

        assertEquals(expectedOutput, observerOutput)
    }

    @Test
    fun printMappingValues() {
        val startInputValue = 0
        val endInputValue = 100

        for (inputValue in startInputValue..endInputValue) {
            println("Input - $inputValue is ${subject.mapRate(inputValue)}")
        }
    }
}
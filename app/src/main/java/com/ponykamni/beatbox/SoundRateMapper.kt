package com.ponykamni.beatbox

class SoundRateMapper {

    fun mapRate(seekBarRate: Int): Float {

        if (seekBarRate > 100 || seekBarRate < 0) {
            return DEFAULT_OUTPUT
        }

        return if (seekBarRate > 50) {
            mapAboveDefault(seekBarRate)
        } else {
            mapUnderDefault(seekBarRate)
        }
    }

    private fun mapUnderDefault(seekBarRate: Int): Float {
        val seekBarRateFloat = seekBarRate.toFloat()

        val rateFloatPlusFifty = seekBarRateFloat + 50

        return rateFloatPlusFifty / 100
    }

    private fun mapAboveDefault(seekBarRate: Int): Float {
        val seekBarRateFloat = seekBarRate.toFloat()

        return seekBarRateFloat * 2 / 100
    }

    companion object {
        private const val DEFAULT_OUTPUT = 1.0f
    }
}